﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Diagnostics;
using System.IO;

class GenerateTerrain : EditorWindow
{
    //GUI VARIABLES
    string terrainName = "Unit _";
    string DEMFilePath = "";
    bool DEMBrowseClick = false;
    string OrthoFilePath = "";
    bool OrthoBrowseClick = false;
    bool submitClick = false;

    //TERRAIN INFO
    string UpperLeftE;
    string UpperLeftN;
    string UpperRightE;
    string UpperRightN;
    string LowerLeftE;
    string LowerLeftN;
    string LowerRightE;
    string LowerRightN;
    string MinHeight;
    string MaxHeight;

    [MenuItem("TerrainBuilder/Generate Terrain")]

    public static void ShowWindow()
    {
        EditorWindow.GetWindowWithRect(typeof(GenerateTerrain), new Rect(0, 0, 400, 175));
    }

    /*
     *Executes commandline gdal_translate and image magick 
     */
    public void runGDAL_Translate(string DEMfilepath, string HMfilepath)
    {
        //GDAL_TRANSLATE
        Process cmdt = new Process();
        cmdt.StartInfo.FileName = "cmd.exe";
        cmdt.StartInfo.RedirectStandardInput = true;
        cmdt.StartInfo.RedirectStandardOutput = true;
        cmdt.StartInfo.CreateNoWindow = true;
        cmdt.StartInfo.UseShellExecute = false;
        cmdt.Start();
        cmdt.StandardInput.WriteLine(@"gdal_translate -ot Byte -scale " + MinHeight + " " + MaxHeight + @" 0 255 -of png " + DEMfilepath + " " + Application.dataPath + "/TerrainFiles/" + HMfilepath);
        cmdt.StandardInput.Flush();

        //IMAGE MAGICK CONVERT ORTHO TIFF TO PNG OF EACH PYRAMID IMAGE
        //CURRENTLY ONLY USES ORTHO[0].PNG BUT CAN CONVERT ALL TEXTURES BY REPLACING WITH FOLLOWING
        //cmdt.StandardInput.WriteLine(@"magick convert " + OrthoFilePath + " " + Application.dataPath + "/TerrainFiles/Textures/" + terrainName.Replace(" ", "") + "Ortho%d.png");
        cmdt.StandardInput.WriteLine(@"magick convert " + OrthoFilePath + "[0] " + Application.dataPath + "/TerrainFiles/Textures/" + terrainName.Replace(" ", "") + "Ortho0.png");
        cmdt.StandardInput.Flush();
        cmdt.StandardInput.WriteLine("exit");
        cmdt.StandardInput.Flush();
        cmdt.StandardInput.Close();
        cmdt.WaitForExit();
        cmdt.Dispose();

        //CALL TO GENERATE TERRAIN OBJECT
        generateTerrainObj();
    }

    /*
     * Executes gdalinfo on commandline and stores relevant information
     */
    public void runGDALInfo(string DEMfilepath)
    {
        //RUN GDAL INFO TO READ GEOTIFF INFORMATION
        Process cmd = new Process();
        cmd.StartInfo.FileName = "cmd.exe";
        cmd.StartInfo.RedirectStandardInput = true;
        cmd.StartInfo.RedirectStandardOutput = true;
        cmd.StartInfo.CreateNoWindow = true;
        cmd.StartInfo.UseShellExecute = false;
        cmd.Start();

        cmd.StandardInput.WriteLine(@"gdalinfo -mm " + DEMfilepath);
        cmd.StandardInput.Flush();
        cmd.StandardInput.WriteLine("exit");
        cmd.StandardInput.Flush();

        //OUTPUT FROM GDALINFO 
        string output = cmd.StandardOutput.ReadToEnd();
        
        string[] lines = output.Split('\n');

        //GET INFORMATION FROM GDALINFO OUTPUT AND STORE ACCORDINGLY
        MinHeight = lines[40].Split('=', '=', ' ')[3];
        MaxHeight = lines[40].Split('=', '=', ' ')[5];

        UpperLeftE = lines[34].Split(' ', '(', ',', ')')[6];
        UpperLeftN = lines[34].Split(' ', '(', ',', ')')[8];
        LowerLeftE = lines[35].Split(' ', '(', ',', ')')[6];
        LowerLeftN = lines[35].Split(' ', '(', ',', ')')[8];
        UpperRightE = lines[36].Split(' ', '(', ',', ')')[5];
        UpperRightN = lines[36].Split(' ', '(', ',', ')')[7];
        LowerRightE = lines[37].Split(' ', '(', ',', ')')[5];
        LowerRightN = lines[37].Split(' ', '(', ',', ')')[7];
        cmd.Dispose();

        //RUN GDAL TRANSLATE TO GENERATE HEIGHTMAP PNG AFTER GETTING INFO ABOUT TERRAIN
        runGDAL_Translate(DEMfilepath, terrainName.Replace(" ", string.Empty) + "hm.png");
    }

    /*
     * Generates Unity Terrain Object
     */
    void generateTerrainObj() { 
        //CREATE UNITY TERRAIN OBJECT FROM INFO
        TerrainData terrData = new TerrainData();
        GameObject terrGO = Terrain.CreateTerrainGameObject(terrData);

        //ADD GEOCOORDHANDLER CLASS TO POSITION/SCALE RELATIVELY
        terrGO.AddComponent<GeoCoordHandler>();
        Terrain terrObj = terrGO.GetComponent<Terrain>();
        terrObj.name = terrainName;
        terrObj.terrainData.heightmapResolution = 513;
        terrObj.terrainData.baseMapResolution = 1024;

        //APPLY HEIGHTMAP FROM GDALTRANSLATE
        byte[] img = System.IO.File.ReadAllBytes(@"Assets\TerrainFiles\" + terrainName.Replace(" ", string.Empty) + "hm.png");
        Texture2D hm = new Texture2D(2, 2);
        bool loaded = hm.LoadImage(img);
        ApplyHeightmap(hm, terrObj);

        //SET GEOCOORDHANDLER INFO
        setData(terrObj);
        setTexture(terrObj);
    }

    /*
     * Uses PNG Heightmap to create the terrain (Adapted from Heightmap JS from Unity Wiki)
     */
    public void ApplyHeightmap(Texture2D heightmap, Terrain terrobj)
    {
        if (heightmap == null)
        {
            EditorUtility.DisplayDialog("No texture selected", "Please select a texture.", "Cancel");
            return;
        }
        var terrain = terrobj.terrainData;
        int w = heightmap.width;
        int h = heightmap.height;
        int w2 = terrain.heightmapWidth;
        float[,] heightmapData = terrain.GetHeights(0, 0, w2, w2);
        Color[] mapColors = heightmap.GetPixels();
        Color[] map = new Color[w2 * w2];
        if (w2 != w || h != w)
        {
            // Resize using nearest-neighbor scaling if texture has no filtering
            if (heightmap.filterMode == FilterMode.Point)
            {
                float dx = (float)w / (float)w2;
                float dy = (float)h / (float)w2;
                for (int y = 0; y < w2; y++)
                {
                    if (y % 20 == 0)
                    {
                        EditorUtility.DisplayProgressBar("Resize", "Calculating texture", Mathf.InverseLerp(0.0f, w2, y));
                    }
                    int thisY = Mathf.FloorToInt(dy * y) * w;
                    int yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        map[yw + x] = mapColors[Mathf.FloorToInt(thisY + dx * x)];
                    }
                }
            }
            // Otherwise resize using bilinear filtering
            else
            {
                float ratioX = (1.0f / ((float)w2 / (w - 1)));
                float ratioY = (1.0f / ((float)w2 / (h - 1)));
                for (int y = 0; y < w2; y++)
                {
                    if (y % 20 == 0)
                    {
                        EditorUtility.DisplayProgressBar("Resize", "Calculating texture", Mathf.InverseLerp(0.0f, w2, y));
                    }
                    int yy = Mathf.FloorToInt(y * ratioY);
                    int y1 = yy * w;
                    int y2 = (yy + 1) * w;
                    int yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        int xx = Mathf.FloorToInt(x * ratioX);
                        Color bl = mapColors[y1 + xx];
                        Color br = mapColors[y1 + xx + 1];
                        Color tl = mapColors[y2 + xx];
                        Color tr = mapColors[y2 + xx + 1];
                        float xLerp = x * ratioX - xx;
                        map[yw + x] = Color.Lerp(Color.Lerp(bl, br, xLerp), Color.Lerp(tl, tr, xLerp), y * ratioY - (float)yy);
                    }
                }
            }
            EditorUtility.ClearProgressBar();
        }
        else
        {
            // Use original if no resize is needed
            map = mapColors;
        }
        // Assign texture data to heightmap
        for (int y = 0; y < w2; y++)
        {
            for (int x = 0; x < w2; x++)
            {
                heightmapData[y, x] = map[y * w2 + x].grayscale;
            }
        }
        terrain.SetHeights(0, 0, heightmapData);
    }

    /*
     * Attaches GeoTiff Info to the Terrain Unity Object and repositions accordingly
     */
    public void setData(Terrain terrObj)
    {
        GeoCoordHandler terrGCH = terrObj.GetComponent<GeoCoordHandler>();
        terrGCH.UpperLeftE = UpperLeftE;
        terrGCH.UpperLeftN = UpperLeftN;
        terrGCH.UpperRightE = UpperRightE;
        terrGCH.UpperRightN = UpperRightN;
        terrGCH.LowerLeftE = LowerLeftE;
        terrGCH.LowerLeftN = LowerLeftN;
        terrGCH.LowerRightE = LowerRightE;
        terrGCH.LowerRightN = LowerRightN;
        terrGCH.MinHeight = MinHeight;
        terrGCH.MaxHeight = MaxHeight;

        terrGCH.UseCoords(terrObj.terrainData);
    }

    /*
     * Adds texture to terrain from Ortho PNG file
     */
    void setTexture(Terrain terrObj)
    {
        byte[] img = System.IO.File.ReadAllBytes(@"Assets\TerrainFiles\Textures\" + terrainName.Replace(" ", string.Empty) + "Ortho0.png");
        Texture2D terrTex = new Texture2D(2, 2);
        terrTex.LoadImage(img);
        terrTex.alphaIsTransparency = true;
        SplatPrototype[] tex = new SplatPrototype[1];
        tex[0] = new SplatPrototype();
        tex[0].texture = terrTex;
        tex[0].tileSize = new Vector2(terrObj.terrainData.size.x, terrObj.terrainData.size.z);
        terrObj.terrainData.splatPrototypes = tex;

    }

    /*
     * The Following functions are for the GUI
     */
    void onDEMBrowseClick()
    {
        DEMFilePath = EditorUtility.OpenFilePanel("Select DEM", "", "tif");
        DEMBrowseClick = false;
    }

    void onOrthoBrowseClick()
    {
        OrthoFilePath = EditorUtility.OpenFilePanel("Select Ortho", "", "tif");
        OrthoBrowseClick = false;
    }

    void onSubmitClick()
    {
        runGDALInfo(DEMFilePath);
        this.Close();
        submitClick = false;
    }

    void OnGUI()
    {
        // The actual window code goes here
        GUILayout.Label("Upload Unit Files to Generate Terrain", EditorStyles.boldLabel);

        terrainName = EditorGUILayout.TextField("Terrain Name", terrainName);

        //DEM File Selection
        GUILayout.BeginHorizontal();
        DEMFilePath = EditorGUILayout.TextField("DEM File", DEMFilePath);
        DEMBrowseClick = GUILayout.Button("Browse...", GUILayout.MaxWidth(70));
        GUILayout.EndHorizontal();

       //Ortho File Selection
        GUILayout.BeginHorizontal();
        OrthoFilePath = EditorGUILayout.TextField("Ortho File", OrthoFilePath);
        OrthoBrowseClick = GUILayout.Button("Browse...", GUILayout.MaxWidth(70));
        GUILayout.EndHorizontal();
        
        //Submission
        submitClick = GUILayout.Button("Generate Terrain with Files");

        //DEM Browse
        if (DEMBrowseClick)
        {
            onDEMBrowseClick();
        }

        //Ortho Browse
        if (OrthoBrowseClick)
        {
            onOrthoBrowseClick();
        }

        //Submit Click
        if (submitClick)
        {
            onSubmitClick();
        }
    }
}