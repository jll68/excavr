﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Diagnostics;
using System.IO;

class GenerateShapeUnit : EditorWindow
{
    string terrainName = "Unit _";
    string DEMFilePath = "";
    bool DEMBrowseClick = false;
    string OrthoFilePath = "";
    bool OrthoBrowseClick = false;
    string ShapeFilePath = "";
    bool ShapeBrowseClick = false;
    bool submitClick = false;

    string UpperLeftE;
    string UpperLeftN;
    string UpperRightE;
    string UpperRightN;
    string LowerLeftE;
    string LowerLeftN;
    string LowerRightE;
    string LowerRightN;
    string MinHeight;
    string MaxHeight;

    string[] shape;


    private class Point
    {
        public int X;
        public int Y;

        public Point(int newX, int newY)
        {
            X = newX;
            Y = newY;
        }
    }
    [MenuItem("TerrainBuilder/Generate Shape Unit")]

    public static void ShowWindow()
    {
        EditorWindow.GetWindowWithRect(typeof(GenerateShapeUnit), new Rect(0, 0, 400, 175));
    }

    void onDEMBrowseClick()
    {
        DEMFilePath = EditorUtility.OpenFilePanel("Select DEM", "", "tif");
        DEMBrowseClick = false;
    }

    void onOrthoBrowseClick()
    {
        OrthoFilePath = EditorUtility.OpenFilePanel("Select Ortho", "", "tif");
        OrthoBrowseClick = false;
    }

    void onShapeBrowseClick()
    {
        ShapeFilePath = EditorUtility.OpenFilePanel("Select Shape File", "", "shp");
        ShapeBrowseClick = false;
    }

    void onSubmitClick()
    {
        
        runGDALInfo(DEMFilePath);
        this.Close();
        submitClick = false;
    }

    void OnGUI()
    {
        // The actual window code goes here
        GUILayout.Label("Upload Unit & Shape Files to Generate Shape Unit", EditorStyles.boldLabel);

        terrainName = EditorGUILayout.TextField("Terrain Name", terrainName);

        //DEM File Selection
        GUILayout.BeginHorizontal();
        DEMFilePath = EditorGUILayout.TextField("DEM File", DEMFilePath);
        DEMBrowseClick = GUILayout.Button("Browse...", GUILayout.MaxWidth(70));
        GUILayout.EndHorizontal();

        //Ortho File Selection
        GUILayout.BeginHorizontal();
        OrthoFilePath = EditorGUILayout.TextField("Ortho File", OrthoFilePath);
        OrthoBrowseClick = GUILayout.Button("Browse...", GUILayout.MaxWidth(70));
        GUILayout.EndHorizontal();

        //Shape File Selection
        GUILayout.BeginHorizontal();
        ShapeFilePath = EditorGUILayout.TextField("Shape File", ShapeFilePath);
        ShapeBrowseClick = GUILayout.Button("Browse...", GUILayout.MaxWidth(70));
        GUILayout.EndHorizontal();

        //Submission
        submitClick = GUILayout.Button("Generate Terrain with Files");

        //DEM Browse
        if (DEMBrowseClick)
        {
            onDEMBrowseClick();
        }

        //Ortho Browse
        if (OrthoBrowseClick)
        {
            onOrthoBrowseClick();
        }

        //Shape File Browse
        if (ShapeBrowseClick)
        {
            onShapeBrowseClick();
        }

        //Submit Click
        if (submitClick)
        {
            onSubmitClick();
        }
    }

    /*
     * Executes GDALINFO on the command line and stores information
     */
    public void runGDALInfo(string DEMfilepath)
    {
        Process cmd = new Process();
        cmd.StartInfo.FileName = "cmd.exe";
        cmd.StartInfo.RedirectStandardInput = true;
        cmd.StartInfo.RedirectStandardOutput = true;
        cmd.StartInfo.CreateNoWindow = true;
        cmd.StartInfo.UseShellExecute = false;
        cmd.Start();

        cmd.StandardInput.WriteLine(@"gdalinfo -mm " + DEMfilepath);
        cmd.StandardInput.Flush();
        cmd.StandardInput.WriteLine("exit");
        cmd.StandardInput.Flush();

        //store information from the output
        string output = cmd.StandardOutput.ReadToEnd();
        string[] lines = output.Split('\n');
        MinHeight = lines[40].Split('=', '=', ' ')[3];
        MaxHeight = lines[40].Split('=', '=', ' ')[5];
        UpperLeftE = lines[34].Split(' ', '(', ',', ')')[6];
        UpperLeftN = lines[34].Split(' ', '(', ',', ')')[8];
        LowerLeftE = lines[35].Split(' ', '(', ',', ')')[6];
        LowerLeftN = lines[35].Split(' ', '(', ',', ')')[8];
        UpperRightE = lines[36].Split(' ', '(', ',', ')')[5];
        UpperRightN = lines[36].Split(' ', '(', ',', ')')[7];
        LowerRightE = lines[37].Split(' ', '(', ',', ')')[5];
        LowerRightN = lines[37].Split(' ', '(', ',', ')')[7];
        cmd.Dispose();
        
        //Use information to run gdal_translate
        runGDAL_Translate(DEMfilepath, terrainName.Replace(" ", string.Empty) + "hm.png");
    }

    /*
     * Executes commandline ogrinfo to get shape coordinates
     */
    void runShapeReader()
    {
        Process cmd = new Process();
        cmd.StartInfo.FileName = "cmd.exe";
        cmd.StartInfo.RedirectStandardInput = true;
        cmd.StartInfo.RedirectStandardOutput = true;
        cmd.StartInfo.CreateNoWindow = true;
        cmd.StartInfo.UseShellExecute = false;
        cmd.Start();

        cmd.StandardInput.WriteLine(@"ogrinfo -al " + ShapeFilePath);
        cmd.StandardInput.Flush();
        cmd.StandardInput.WriteLine("exit");
        cmd.StandardInput.Flush();
        string output = cmd.StandardOutput.ReadToEnd();
        cmd.Dispose();
        //Save output information
        string[] lines = output.Split('\n');
        string[] pts = lines[41].Split('(', ')')[2].Split(',');
        shape = pts;

        
        generateTerrainObj();
    }

    /*
     * Executes GDAL_TRANSLATE, checks if relevant files already generated
     */
    public void runGDAL_Translate(string DEMfilepath, string HMfilepath)
    {
        //Check for hm and texture
        bool hmExists = false;
        bool orthoExists = false;
        if(File.Exists(@"Assets\TerrainFiles\" + terrainName.Replace(" ", string.Empty) + "hm.png"))
        {
            hmExists = true;
        }
        if (File.Exists(@"Assets\TerrainFiles\Textures\" + terrainName.Replace(" ", string.Empty) + "Ortho0.png"))
        {
            orthoExists = true;
        }
        //GDAL_TRANSLATE
        if(!orthoExists && !hmExists)
        {
            Process cmdt = new Process();
            cmdt.StartInfo.FileName = "cmd.exe";
            cmdt.StartInfo.RedirectStandardInput = true;
            cmdt.StartInfo.RedirectStandardOutput = true;
            cmdt.StartInfo.CreateNoWindow = false;
            cmdt.StartInfo.UseShellExecute = false;
            cmdt.Start();
            if(!hmExists)
            {
                cmdt.StandardInput.WriteLine(@"gdal_translate -ot Byte -scale " + MinHeight + " " + MaxHeight + @" 0 255 -of png " + DEMfilepath + " " + Application.dataPath + "/TerrainFiles/" + HMfilepath);
                cmdt.StandardInput.Flush();
            }
            if(!orthoExists)
            {
                cmdt.StandardInput.WriteLine(@"magick convert " + OrthoFilePath + " " + Application.dataPath + "/TerrainFiles/Textures/" + terrainName.Replace(" ", "") + "Ortho%d.png");
                cmdt.StandardInput.Flush();
            }
            cmdt.StandardInput.WriteLine("exit");
            cmdt.StandardInput.Flush();
            cmdt.StandardInput.Close();
            cmdt.WaitForExit();
            cmdt.Dispose();
        }

        runShapeReader();
    }

    /*
     * Creates Unity Terrain Object
     */
    void generateTerrainObj()
    {

        TerrainData terrData = new TerrainData();
        GameObject terrGO = Terrain.CreateTerrainGameObject(terrData);
        terrGO.AddComponent<GeoCoordHandler>();
        Terrain terrObj = terrGO.GetComponent<Terrain>();
        terrObj.name = terrainName + "Shape Unit";
        terrObj.terrainData.heightmapResolution = 513;
        terrObj.terrainData.baseMapResolution = 1024;
        byte[] img = System.IO.File.ReadAllBytes(@"Assets\TerrainFiles\" + terrainName.Replace(" ", string.Empty) + "hm.png");
        Texture2D hm = new Texture2D(2, 2);
        bool loaded = hm.LoadImage(img);

        //Getting the coordinates of the shape file and translating it to pixel values on the heightmap image
        int pixX = hm.width;
        int pixY = hm.height;
        Point[] polygon = new Point[shape.Length];
        for (int i = 0; i < shape.Length; i++)
        {
            string[] xyz = shape[i].Split(' ');
            float ptTerrX = getShiftedNum(xyz[0], "716280");
            float ptTerrY = getShiftedNum(xyz[1], "4699660");
            int ptX = getPixelValueX(ptTerrX, pixX);
            int ptY = getPixelValueY(ptTerrY, pixY);
            polygon[i] = new Point(ptX, ptY);
        }

        //if the pixel falls outside the shape, color it black
        Color[] pix = hm.GetPixels(); // get pixel colors
        for (int i = 0; i < pixX; i++)
        {
            for (int j = 0; j < pixY; j++)
            {
                Point tmpPt = new Point(i, j);
                if(!IsPointInPolygon(polygon, tmpPt))
                {
                    pix[j * pixX + i] = Color.black;
                }
            }
        }
        
        //set the new pixel values
        hm.SetPixels(pix); // set changed pixel alphas
        hm.Apply(); // upload texture to GPU

        //use cut out heightmap to render terrain
        ApplyHeightmap(hm, terrObj);

        setData(terrObj);
        setTexture(terrObj);
    }

    /*
     * Using the Terrain GeoLoc X Value (ptTerrX) and the Width of the img file (texWidth), find the corresponding X value of the pixel
     */
    int getPixelValueX(float ptTerrX, int texWidth)
    {
        float leftTerrX = getShiftedNum(UpperLeftE, "716280");
        float rightTerrX = getShiftedNum(UpperRightE, "716280");
        float TerrWidth = rightTerrX - leftTerrX;
        float PtDiff = ptTerrX - leftTerrX;
        float PtPixel = (PtDiff / TerrWidth) * texWidth;
        return (int)System.Math.Round(PtPixel);
    }

    /*
     * Using the Terrain GeoLoc Y Value (ptTerrY) and the Height of the img file (texHeight), find the corresponding Y value of the pixel
     */
    int getPixelValueY(float ptTerrY, int texHeight)
    {
        float upTerrY = getShiftedNum(UpperLeftN, "4699660");
        float lowTerrY = getShiftedNum(LowerLeftN, "4699660");
        float TerrWidth = upTerrY - lowTerrY;
        float PtDiff = ptTerrY - lowTerrY;
        float PtPixel = (PtDiff / TerrWidth) * texHeight;
        return (int)System.Math.Round(PtPixel);
    }

    /*
     * Returns the Float Value of the String Location Value (GeoVal) relative to Value to shift by (shiftNum)
     */
    float getShiftedNum(string GeoVal, string ShiftNum)
    {
        int index = FindDiffIndex(GeoVal, ShiftNum);
        return float.Parse(GeoVal.Substring(index), System.Globalization.CultureInfo.InvariantCulture) - float.Parse(ShiftNum.Substring(index), System.Globalization.CultureInfo.InvariantCulture);
    }

    /*
     * Since the string values are long and the differences are small (generally less than 2m). Find the index at which they are different.
     */
    int FindDiffIndex(string Num, string SubNum)
    {
        int i = 0;
        while (Num.Length > 1 & SubNum.Length > 1 & Num.Substring(i, 1).Equals(SubNum.Substring(i, 1)) & i < System.Math.Min(SubNum.Length, Num.Length) - 1)
        {
            i++;
        }
        return i;
    }

    /*
     * Uses PNG Heightmap to create the terrain (Adapted from Heightmap JS from Unity Wiki)
     */
    public void ApplyHeightmap(Texture2D heightmap, Terrain terrobj)
    {
        if (heightmap == null)
        {
            EditorUtility.DisplayDialog("No texture selected", "Please select a texture.", "Cancel");
            return;
        }
        var terrain = terrobj.terrainData;
        int w = heightmap.width;
        int h = heightmap.height;
        int w2 = terrain.heightmapWidth;
        float[,] heightmapData = terrain.GetHeights(0, 0, w2, w2);
        Color[] mapColors = heightmap.GetPixels();
        Color[] map = new Color[w2 * w2];
        if (w2 != w || h != w)
        {
            // Resize using nearest-neighbor scaling if texture has no filtering
            if (heightmap.filterMode == FilterMode.Point)
            {
                float dx = (float)w / (float)w2;
                float dy = (float)h / (float)w2;
                for (int y = 0; y < w2; y++)
                {
                    if (y % 20 == 0)
                    {
                        EditorUtility.DisplayProgressBar("Resize", "Calculating texture", Mathf.InverseLerp(0.0f, w2, y));
                    }
                    int thisY = Mathf.FloorToInt(dy * y) * w;
                    int yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        map[yw + x] = mapColors[Mathf.FloorToInt(thisY + dx * x)];
                    }
                }
            }
            // Otherwise resize using bilinear filtering
            else
            {
                float ratioX = (1.0f / ((float)w2 / (w - 1)));
                float ratioY = (1.0f / ((float)w2 / (h - 1)));
                for (int y = 0; y < w2; y++)
                {
                    if (y % 20 == 0)
                    {
                        EditorUtility.DisplayProgressBar("Resize", "Calculating texture", Mathf.InverseLerp(0.0f, w2, y));
                    }
                    int yy = Mathf.FloorToInt(y * ratioY);
                    int y1 = yy * w;
                    int y2 = (yy + 1) * w;
                    int yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        int xx = Mathf.FloorToInt(x * ratioX);
                        Color bl = mapColors[y1 + xx];
                        Color br = mapColors[y1 + xx + 1];
                        Color tl = mapColors[y2 + xx];
                        Color tr = mapColors[y2 + xx + 1];
                        float xLerp = x * ratioX - xx;
                        map[yw + x] = Color.Lerp(Color.Lerp(bl, br, xLerp), Color.Lerp(tl, tr, xLerp), y * ratioY - (float)yy);
                    }
                }
            }
            EditorUtility.ClearProgressBar();
        }
        else
        {
            // Use original if no resize is needed
            map = mapColors;
        }
        // Assign texture data to heightmap
        for (int y = 0; y < w2; y++)
        {
            for (int x = 0; x < w2; x++)
            {
                heightmapData[y, x] = map[y * w2 + x].grayscale;
            }
        }
        terrain.SetHeights(0, 0, heightmapData);
    }

    /*
     * Attaches GeoTiff Info to the Terrain Unity Object and repositions accordingly
     */
    public void setData(Terrain terrObj)
    {
        GeoCoordHandler terrGCH = terrObj.GetComponent<GeoCoordHandler>();
        terrGCH.UpperLeftE = UpperLeftE;
        terrGCH.UpperLeftN = UpperLeftN;
        terrGCH.UpperRightE = UpperRightE;
        terrGCH.UpperRightN = UpperRightN;
        terrGCH.LowerLeftE = LowerLeftE;
        terrGCH.LowerLeftN = LowerLeftN;
        terrGCH.LowerRightE = LowerRightE;
        terrGCH.LowerRightN = LowerRightN;
        terrGCH.MinHeight = MinHeight;
        terrGCH.MaxHeight = MaxHeight;

        terrGCH.UseCoords(terrObj.terrainData);
    }

    /*
     * Adds texture to terrain from Ortho PNG file
     */
    void setTexture(Terrain terrObj)
    {
        byte[] img = System.IO.File.ReadAllBytes(@"Assets\TerrainFiles\Textures\" + terrainName.Replace(" ", string.Empty) + "Ortho0.png");
        Texture2D terrTex = new Texture2D(2, 2);
        terrTex.LoadImage(img);
        terrTex.alphaIsTransparency = true;
        int pixX = terrTex.width;
        int pixY = terrTex.height;

        //GETS SHAPE FROM SHAPEFILE POINTS AND CUTS OUT SHAPE FROM TEXTURE IMAGE
        Point[] polygon = new Point[shape.Length];
        for (int i = 0; i < shape.Length; i++)
        {
            string[] xyz = shape[i].Split(' ');
            float ptTerrX = getShiftedNum(xyz[0], "716280");
            float ptTerrY = getShiftedNum(xyz[1], "4699660");
            int ptX = getPixelValueX(ptTerrX, pixX);
            int ptY = getPixelValueY(ptTerrY, pixY);
            polygon[i] = new Point(ptX, ptY);
        }
        Color[] pix = terrTex.GetPixels(); // get pixel colors
        for (int i = 0; i < pixX; i++)
        {
            for (int j = 0; j < pixY; j++)
            {
                Point tmpPt = new Point(i, j);
                if (!IsPointInPolygon(polygon, tmpPt))
                {
                    pix[j * pixX + i] = Color.black;
                }
            }
        }
        terrTex.SetPixels(pix); // set changed pixel alphas
        terrTex.Apply(); // upload texture to GPU

        SplatPrototype[] tex = new SplatPrototype[1];
        tex[0] = new SplatPrototype();
        tex[0].texture = terrTex;
        tex[0].tileSize = new Vector2(terrObj.terrainData.size.x, terrObj.terrainData.size.z);
        terrObj.terrainData.splatPrototypes = tex;

    }

    /*
     * FROM STACK OVERFLOW, CHECKS IF PT IN POLYGON
     */
    private bool IsPointInPolygon(Point[] polygon, Point point)
    {
        bool isInside = false;
        for (int i = 0, j = polygon.Length - 1; i < polygon.Length; j = i++)
        {
            if (((polygon[i].Y > point.Y) != (polygon[j].Y > point.Y)) &&
            (point.X < (polygon[j].X - polygon[i].X) * (point.Y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) + polygon[i].X))
            {
                isInside = !isInside;
            }
        }
        return isInside;
    }
}
 