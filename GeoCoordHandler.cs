﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeoCoordHandler : MonoBehaviour {
    public string UpperLeftE;
    public string UpperLeftN;
    public string UpperRightE;
    public string UpperRightN;
    public string LowerLeftE;
    public string LowerLeftN;
    public string LowerRightE;
    public string LowerRightN;
    public string CenterE;
    public string CenterN;
    public string MinHeight;
    public string MaxHeight;
    public string ESubNum = "716280";
    public string NSubNum = "4699660";
    public string HSubNum = "65";

    // Use this for initialization
    void Start () {
       ShiftLoc();

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    
    /*
     * Shifts the Location of the Object Relative to the Decided Origin (716280, 4699660, 65)
     */
    void ShiftLoc()
    {
        
        int indexLLE = FindDiffIndex(LowerLeftE, ESubNum);
        float NewLowerLeftX = float.Parse(LowerLeftE.Substring(indexLLE), System.Globalization.CultureInfo.InvariantCulture) - float.Parse(ESubNum.Substring(indexLLE), System.Globalization.CultureInfo.InvariantCulture);

        int indexLLN = FindDiffIndex(LowerLeftN, NSubNum);
        float NewLowerLeftZ = float.Parse(LowerLeftN.Substring(indexLLN), System.Globalization.CultureInfo.InvariantCulture) - float.Parse(NSubNum.Substring(indexLLN), System.Globalization.CultureInfo.InvariantCulture);

        int indexH = FindDiffIndex(MinHeight, HSubNum);
		float newHeight = float.Parse(MinHeight.Substring(indexH), System.Globalization.CultureInfo.InvariantCulture) - float.Parse(HSubNum.Substring(indexH), System.Globalization.CultureInfo.InvariantCulture);
        this.gameObject.transform.position = new Vector3(NewLowerLeftX, newHeight, NewLowerLeftZ);
    }

    /*
     * Returns the Float Value of the String Location Value (GeoVal) relative to Value to shift by (shiftNum)
     */
    float getShiftedNum(string GeoVal, string ShiftNum)
    {
        int index = FindDiffIndex(GeoVal, ShiftNum);
        return float.Parse(GeoVal.Substring(index), System.Globalization.CultureInfo.InvariantCulture) - float.Parse(ShiftNum.Substring(index), System.Globalization.CultureInfo.InvariantCulture);
    }

    /*
     * Since the string values are long and the differences are small (generally less than 2m). Find the index at which they are different.
     */

    int FindDiffIndex(string Num, string SubNum)
    {
        int i = 0;
		while ( Num.Length > 1& SubNum.Length > 1&Num.Substring(i,1).Equals(SubNum.Substring(i,1)) & i < System.Math.Min(SubNum.Length, Num.Length)-1){
            i++;
        }
        return i;
    }

    /*
     * Scale the terrain according to the terrain height, width, and length info
     */
    void ScaleTerrain(TerrainData tData)
    {
        //tData.size.Set()

        //Get Width
        float width_x = getShiftedNum(UpperRightE, UpperLeftE);

        //Get Length
        float length_z = getShiftedNum(UpperLeftN, LowerLeftN);

        //Get Height
        float height_y = getShiftedNum(MaxHeight, MinHeight);

         tData.size = new Vector3(width_x, height_y, length_z);
        
    }

    /*
     * Shift and Scale the terrain object 
     */

    public void UseCoords(TerrainData tData)
    {
        ShiftLoc();
        ScaleTerrain(tData);
    }
}
