﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Diagnostics;

class OutlineShape : EditorWindow
{
    string ShapeFilePath = "";
    bool ShapeBrowseClick = false;
    bool submitClick = false;
    Object Unit;

    private OutlineShape window;

    [MenuItem("TerrainBuilder/Outline Shape")]

    public static void ShowWindow()
    {
        EditorWindow.GetWindowWithRect(typeof(OutlineShape), new Rect(0, 0, 400, 125));
    }

    void onShapeBrowseClick()
    {
        ShapeFilePath = EditorUtility.OpenFilePanel("Select Shape File", "", "shp");
        ShapeBrowseClick = false;
    }

    void onSubmitClick()
    {
        runShapeReader();
        this.Close();
        submitClick = false;
    }

    void OnGUI()
    {
        GUILayout.Label("Upload Shape File to Generate Outline", EditorStyles.boldLabel);
        EditorGUILayout.Space();
        Unit = EditorGUILayout.ObjectField("Unit Object", Unit, typeof(Object), true);
        //Shape File Selection
        GUILayout.BeginHorizontal();
        ShapeFilePath = EditorGUILayout.TextField("Shape File", ShapeFilePath);
        ShapeBrowseClick = GUILayout.Button("Browse...", GUILayout.MaxWidth(70));
        GUILayout.EndHorizontal();
        EditorGUILayout.Space();
        //Submission
        submitClick = GUILayout.Button("Generate Shape Outline with Files");

        //Shape File Browse
        if (ShapeBrowseClick)
        {
            onShapeBrowseClick();
        }
        
        //Submit Click
        if (submitClick)
        {
            onSubmitClick();
        }
    }

    /*
     * Executes ogrinfo on commandline to get coordinates from shape file
     */
    void runShapeReader()
    {
        Process cmd = new Process();
        cmd.StartInfo.FileName = "cmd.exe";
        cmd.StartInfo.RedirectStandardInput = true;
        cmd.StartInfo.RedirectStandardOutput = true;
        cmd.StartInfo.CreateNoWindow = true;
        cmd.StartInfo.UseShellExecute = false;
        cmd.Start();

        cmd.StandardInput.WriteLine(@"ogrinfo -al " + ShapeFilePath);
        cmd.StandardInput.Flush();
        cmd.StandardInput.WriteLine("exit");
        cmd.StandardInput.Flush();
        string output = cmd.StandardOutput.ReadToEnd();
        cmd.Dispose();
        string[] lines = output.Split('\n');
        string[] pts = lines[41].Split('(', ')')[2].Split(',');
        genLineRenderer(pts);

        
    }

    /*
     * Generates a Unity Line Renderer object that traces out the shape from the shapefile
     */

    void genLineRenderer(string[] pts)
    {
        GameObject gameobject = new GameObject(Unit.name + "shape outline");
        GameObject unit_go = Unit as GameObject;
        gameobject.transform.parent = unit_go.transform;
        LineRenderer lineRenderer = gameobject.AddComponent<LineRenderer>();
        lineRenderer.widthMultiplier = 0.01f;
        lineRenderer.positionCount = pts.Length + 1;

        for (int i = 0; i < pts.Length; i++)
        {
            string[] xyz = pts[i].Split(' ');
            lineRenderer.SetPosition(i, new Vector3(getShiftedNum(xyz[0], "716280"), getShiftedNum(xyz[2], "65"), getShiftedNum(xyz[1], "4699660")));
        }
        
        lineRenderer.SetPosition(pts.Length, lineRenderer.GetPosition(0));

    }

    /*
     * Returns the Float Value of the String Location Value (GeoVal) relative to Value to shift by (shiftNum)
     */

    float getShiftedNum(string GeoVal, string ShiftNum)
    {
        int index = FindDiffIndex(GeoVal, ShiftNum);
        return float.Parse(GeoVal.Substring(index), System.Globalization.CultureInfo.InvariantCulture) - float.Parse(ShiftNum.Substring(index), System.Globalization.CultureInfo.InvariantCulture);
    }

    /*
     * Since the string values are long and the differences are small (generally less than 2m). Find the index at which they are different.
     */
    int FindDiffIndex(string Num, string SubNum)
    {
        int i = 0;
        while (Num.Length > 1 & SubNum.Length > 1 & Num.Substring(i, 1).Equals(SubNum.Substring(i, 1)) & i < System.Math.Min(SubNum.Length, Num.Length) - 1)
        {
            i++;
        }
        return i;
    }
}